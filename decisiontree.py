"""
Decision trees!
"""

# import matplotlib.pyplot as plt
# import networkx as nx
import uuid

TRAINING_SET = [
	{"body_temperature":"warm", "gives_birth":"yes", "four_legged":"yes", "hibernates":"yes", "mammal":"no"},
	{"body_temperature":"warm", "gives_birth":"yes", "four_legged":"yes", "hibernates":"no", "mammal":"no"},
	{"body_temperature":"warm", "gives_birth":"yes", "four_legged":"no", "hibernates":"yes", "mammal":"yes"},
	{"body_temperature":"warm", "gives_birth":"yes", "four_legged":"no", "hibernates":"no", "mammal":"yes"},
	{"body_temperature":"cold", "gives_birth":"no", "four_legged":"yes", "hibernates":"yes", "mammal":"no"},
	{"body_temperature":"cold", "gives_birth":"no", "four_legged":"yes", "hibernates":"no", "mammal":"no"},
	{"body_temperature":"cold", "gives_birth":"no", "four_legged":"no", "hibernates":"yes", "mammal":"no"},
	{"body_temperature":"cold", "gives_birth":"no", "four_legged":"no", "hibernates":"no", "mammal":"no"},
	{"body_temperature":"warm", "gives_birth":"no", "four_legged":"no", "hibernates":"no", "mammal":"no"},
	{"body_temperature":"cold", "gives_birth":"yes", "four_legged":"no", "hibernates":"no", "mammal":"no"},
]

ATTRIBUTES = {
	"body_temperature":["warm", "cold"],
	"gives_birth":["yes", "no"],
	"four_legged":["yes", "no"],
	"hibernates":["yes", "no"]
}
TARGET_CLASS = "mammal"
TARGET_CLASS_OUTCOMES = ["yes", "no"]
GRAPH_FILE = "graph.gv"

class Node(object):
	def __init__(self, test_cond, label):
		self.test_cond = test_cond
		self.label = label
		self.children = list()
		self.parent = None
	
	def addChild(self, child, outcome):
		self.children.append((child, outcome))
	
# 	def __str__(self):
# 		result = str()
# 		if self.label:
# 			result += "Leaf: "+self.label+"\n"
# 		elif self.test_cond:
# 			result += "Test Condition: "+self.test_cond+"\n"
# 			for child, outcome in self.children:
# 				result += "\tChild: "+outcome+"\n\t\t"+child.__str__()+"\n"
# 		return result
# 	
# 	def __repr__(self):
# 		return self.__str__()

def PreProcessGraph(node, graphFile, targetClass):
	node.id = str(uuid.uuid4()).replace("-", "")
	if node.label:
		# leaf node
		graphFile.write("\tleafNode"+node.id+" [label=\""+targetClass+": "+node.label+"\"]\n")
		graphFile.flush()
		return
	elif node.test_cond:
		# define as unique node
		graphFile.write("\tnode"+node.id+" [label=\""+node.test_cond+"\"]\n")
		graphFile.flush()
		for child, outcome in node.children:
			PreProcessGraph(child, graphFile, targetClass)

def DrawGraph(node, graphFile):
	if node.test_cond:
		# normal node.
		for child, outcome in node.children:
			if child.label:
				graphFile.write("\tnode"+node.id+" -> leafNode"+child.id+" [label=\""+outcome+"\"]\n")
				graphFile.flush()
			else:
				graphFile.write("\tnode"+node.id+" -> node"+child.id+" [label=\""+outcome+"\"]\n")
				graphFile.flush()
				DrawGraph(child, graphFile)

def CreateNode():
	return Node(None, None)

def CompareAttributes(entryA, entryB, attributes):
	for attribute in attributes:
		if entryA[attribute] != entryB[attribute]:
			return False

def StoppingCondition(trainingSet, targetClass, attributes):
	checkA = True
	checkB = True
	# same class label?
	for i in range(len(trainingSet)-1):
		if trainingSet[i][targetClass] != trainingSet[i+1][targetClass]:
			checkA = False
	# same attribute values?
	for i in range(len(trainingSet)-1):
		if not CompareAttributes(trainingSet[i], trainingSet[i+1], attributes):
			checkB = False
	return checkA or checkB

def GetClassCounts(trainingSet, targetClassOutcome, targetClass, targetAttributeOutcome, targetAttribute):
	result = 0
	for entry in trainingSet:
		if entry[targetClass] == targetClassOutcome and entry[targetAttribute] == targetAttributeOutcome:
			result += 1
	return result

def Gini(trainingSet, targetAttribute, targetAttributeOutcome, targetClass, targetClassOutcomes):
	result = 1.0
	totalCounts = 0
	for classOutcome in targetClassOutcomes:
		totalCounts += GetClassCounts(trainingSet, classOutcome, targetClass, targetAttributeOutcome, targetAttribute)
	if totalCounts <= 0:
		return (0.0, 0)
	for classOutcome in targetClassOutcomes:
		result -= (GetClassCounts(trainingSet, classOutcome, targetClass, targetAttributeOutcome, targetAttribute)/float(totalCounts))**2
	return (result, totalCounts)

def FindBestSplit(trainingSet, attributes, targetClass, targetClassOutcomes):
	# find the least impurity
	result = str()
	leastImpurity = float("inf")
	# for each attribute...
	for attribute in attributes:
		print "Finding imp for", attribute
		totalCounts = 0
		imp = 0.0
		# for each attribute outcome...
		for attributeOutcome in attributes[attribute]:
			# get the total counts
			g = Gini(trainingSet, attribute, attributeOutcome, targetClass, targetClassOutcomes)
			totalCounts += g[1]
		for attributeOutcome in attributes[attribute]:
			# calculate the weighted sum ginis
			g = Gini(trainingSet, attribute, attributeOutcome, targetClass, targetClassOutcomes)
			imp += (g[1]/float(totalCounts))*g[0]
		print "imp=", imp
		if imp < leastImpurity:
			leastImpurity = imp
			result = attribute
	return result

def GetFrequency(trainingSet, targetClass, targetOutcome):
	result = 0
	for entry in trainingSet:
		if entry[targetClass] == targetOutcome:
			result += 1
	return result

def Classify(trainingSet, targetClass, targetClassOutcomes):
	maxFreq = -1
	result = str()
	for outcome in targetClassOutcomes:
		freq = GetFrequency(trainingSet, targetClass, outcome)
		if freq > maxFreq:
			maxFreq = freq
			result = outcome
	return result

def FilterTrainingSet(trainingSet, testCondition, targetOutcome):
	result = list()
	for entry in trainingSet:
		if entry[testCondition] == targetOutcome:
			result.append(entry)
	return result

def GrowTree(trainingSet, attributes, targetClass, targetClassOutcomes):
	if StoppingCondition(trainingSet, targetClass, attributes):
		leaf = CreateNode()
		leaf.label = Classify(trainingSet, targetClass, targetClassOutcomes)
		return leaf
	else:
		print "Create new node!"
		root = CreateNode()
		print "Finding best split point."
		root.test_cond = FindBestSplit(trainingSet, attributes, targetClass, targetClassOutcomes)
		outcomes = attributes[root.test_cond]
		for outcome in outcomes:
			subset = FilterTrainingSet(trainingSet, root.test_cond, outcome)
			#attributes.pop(root.test_cond, None)
			child = GrowTree(subset, attributes, targetClass, targetClassOutcomes)
			child.parent = root
			root.addChild(child, outcome)
		return root

if __name__ == "__main__":
	print "Its tree time!"
	
	root = GrowTree(TRAINING_SET, ATTRIBUTES, TARGET_CLASS, TARGET_CLASS_OUTCOMES)
	#print root
	with open(GRAPH_FILE, "w") as fp:
		fp.write("digraph {\n")
		PreProcessGraph(root, fp, TARGET_CLASS)
		fp.write("\n")
		DrawGraph(root, fp)
		fp.write("}\n")
	print "Bye bye!"
